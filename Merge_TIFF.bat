@ECHO OFF
ECHO Batch script for merging tiffs
ECHO Author: Marek Szczepkowski
ECHO Date: 21.08.2019
ECHO Version: 1.0
ECHO QGIS 3.0+ (or earlier)


SET QGIS_ROOT=C:\Program Files\QGIS 3.8
SET COORDINATE=3006

REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal

REM Path to working dir
SET WORK=%cd%

ECHO.
set /P c=Are you want to merge TIFs[Y/N]?
if /I "%c%" EQU "Y" goto :merge
if /I "%c%" EQU "N" goto :end
:merge
ECHO.
ECHO Merging tiffs
gdalbuildvrt "%WORK%\Merge.vrt" *.tif 
gdalwarp -s_srs EPSG:%COORDINATE% -co "TFW=YES" -ot Byte -of GTiff -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0" "%WORK%\Merge.vrt" "%WORK%\Merge.tif"
DEL /Q "%WORK%\*.vrt" 

:end
PAUSE